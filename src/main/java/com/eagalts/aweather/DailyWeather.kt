package com.eagalts.aweather

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONObject
import kotlin.math.roundToInt


class DailyWeather : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    private lateinit var recyclerView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val _inflater = inflater.inflate(R.layout.fragment_daily_weather, container, false)

        recyclerView = _inflater.findViewById(R.id.dailyRecyclerView)
        recyclerView.layoutManager = LinearLayoutManager(view?.context)
        requestAPI()

        return _inflater
    }

    private fun parseWeatherData (weatherInfo : String) {
        var flag = false
        val mainObject = JSONObject(weatherInfo)
        val list = mutableListOf<WeatherModel>()

        for (i in 0..39) {

            val item_dt = mainObject.getJSONArray("list").getJSONObject(i).getString("dt")
            val item_timezone = mainObject.getJSONObject("city").getString("timezone")

            val rdate : String = java.time.format.DateTimeFormatter.ISO_INSTANT.format(java.time.Instant.ofEpochSecond(item_dt.toLong() + item_timezone.toLong()))
            val rhour = (rdate[11] + "" + rdate[12]).toInt()
            val rmin = (rdate[14] + "" + rdate[15]).toInt()

            val item_date = mainObject.getJSONArray("list").getJSONObject(i).getString("dt_txt")[8] + "" +
                    mainObject.getJSONArray("list").getJSONObject(i).getString("dt_txt")[9] + "." +
                    mainObject.getJSONArray("list").getJSONObject(i).getString("dt_txt")[5] + "" +
                    mainObject.getJSONArray("list").getJSONObject(i).getString("dt_txt")[6]

                    if (rhour == 12 || (!flag)) {
                if (i <= 35) {
                    val item = WeatherModel(
                        mainObject.getJSONArray("list").getJSONObject(i).getJSONArray("weather").getJSONObject(0).getString("description").capitalize(),
            mainObject.getJSONArray("list").getJSONObject(i).getJSONObject("main").getString("temp").toDouble().roundToInt().toString() + "º",
          mainObject.getJSONArray("list").getJSONObject(i + 4).getJSONObject("main").getString("temp").toDouble().roundToInt().toString() + "º",
                        item_date,
                        item_timezone,
                        mainObject.getJSONArray("list").getJSONObject(i).getJSONArray("weather").getJSONObject(0).getString("icon")
                    )
                    flag = true
                    list.add(item)
                }
            }
        }
        recyclerView.adapter = DailyWeatherAdapter(list)
    }

    private fun requestAPI() {
        val requestURL =
            "https://api.openweathermap.org/data/2.5/forecast?q=$city&appid=$APIKEY&lang=$LANG&units=metric"
        val queue = Volley.newRequestQueue(context)
        lateinit var str : String
        val request = StringRequest(
            Request.Method.GET,
            requestURL,
            {
                    result -> parseWeatherData(result)
            },
            { error ->
                Log.e(
                    "AWeather",
                    "An error occurred while trying to get the weather forecast: $error"
                )
            }
        )

        queue.add(request)
    }
}