package com.eagalts.aweather

import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class HourWeatherAdapter (private val weatherInfo: List<WeatherModel>) : RecyclerView.Adapter<HourWeatherAdapter.MyViewHolder>() {

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val tvDate: TextView = itemView.findViewById(R.id.tvDate)
        val tvCondition: TextView = itemView.findViewById(R.id.tvCondition)
        val tvTemp: TextView = itemView.findViewById(R.id.tvTemp)
        val im: ImageView = itemView.findViewById(R.id.im)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val rdate : String = java.time.format.DateTimeFormatter.ISO_INSTANT.format(java.time.Instant.ofEpochSecond(weatherInfo[position].dt.toLong() + weatherInfo[position].timezone.toLong()))
        val rhour = (rdate[11] + "" + rdate[12]).toInt()
        val rmin = "00"
        val dt = "$rhour:$rmin"

        holder.tvDate.text = dt
        holder.tvCondition.text = weatherInfo[position].state
        holder.tvTemp.text = weatherInfo[position].currentTemp
        setIcon(holder, weatherInfo[position].icon)
    }

    override fun getItemCount() = weatherInfo.size

    private fun setIcon(holder: MyViewHolder, icon : String) {
        when (icon) {
            "01d" -> holder.im.setImageResource(R.drawable.sun_white)
            "01n" -> holder.im.setImageResource(R.drawable.moon_white)
            "02d", "02n" -> holder.im.setImageResource(R.drawable.partly_cloud_white)
            "03d","04d", "03n","04n" -> holder.im.setImageResource(R.drawable.cloud_white)
            "09d","09n" -> holder.im.setImageResource(R.drawable.rain_white)
            "10d","10n" -> holder.im.setImageResource(R.drawable.light_rain_white)
            "11d","11n" -> holder.im.setImageResource(R.drawable.thunder_white)
            "13d","13n" -> holder.im.setImageResource(R.drawable.snow)
            "50d","50n" -> holder.im.setImageResource(R.drawable.mist)
        }
    }

    /*

    @SuppressLint("ResourceAsColor")
    private fun setBgItem (holder: MyViewHolder, icon : String) {
        when (icon) {
            "01d" -> holder.itemBg.setBackgroundColor(R.color.sunnyDay)
            "02d" -> holder.itemBg.setBackgroundColor(R.color.sunnyDay)
            "03d" -> holder.itemBg.setBackgroundColor(R.color.cloudDay)
            "04d" -> holder.itemBg.setBackgroundColor(R.color.cloudDay)
            "09d" -> holder.itemBg.setBackgroundColor(R.color.rainyDay)
            "10d" -> holder.itemBg.setBackgroundColor(R.color.lightRainDay)
            "11d" -> holder.itemBg.setBackgroundColor(R.color.rainyDay)
            "13d" -> holder.itemBg.setBackgroundColor(R.color.snowDay)
            "50d" -> holder.itemBg.setBackgroundColor(R.color.mistDay)
        }
    }

    */
}