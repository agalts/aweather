package com.eagalts.aweather

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.navigation.fragment.findNavController
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.eagalts.aweather.databinding.FragmentMainBinding
import org.json.JSONObject
import kotlin.math.roundToInt

const val APIKEY = ""
lateinit var LANG : String
var city = "Москва"

class MainFragment : Fragment() {

    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentMainBinding.inflate(inflater, container, false)

        LANG = getResources().getString(R.string.locale)

        binding.buttonFirst.setOnClickListener {
            findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment)
        }

        binding.buttonHour.setOnClickListener { hourly() }
        binding.buttonDaily.setOnClickListener { daily() }

        hourly()

        Log.d("AWeather", "Fragment launched")
        return binding.root
    }

    fun setCity(_city : String) { city = _city }

    private fun hourly() {

        binding.loadingPanel.visibility = View.VISIBLE

        val frHour: Fragment = HourWeather()
        val fm = fragmentManager
        val fragmentTransaction: FragmentTransaction = fm!!.beginTransaction()
        fragmentTransaction.replace(binding.fragmentPlace.id, frHour)
        fragmentTransaction.commit()
        binding.buttonHour.setTextColor(resources.getColor(R.color.white))
        binding.buttonDaily.setTextColor(resources.getColor(R.color.black))
    }

    private fun daily() {
        binding.loadingPanel.visibility = View.VISIBLE
        val fr: Fragment = DailyWeather()
        val fm = fragmentManager
        val fragmentTransaction: FragmentTransaction = fm!!.beginTransaction()
        fragmentTransaction.replace(binding.fragmentPlace.id, fr)
        fragmentTransaction.commit()
        binding.buttonDaily.setTextColor(resources.getColor(R.color.white))
        binding.buttonHour.setTextColor(resources.getColor(R.color.black))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requestAPI()
    }

    private fun requestAPI() {
        val requestURL = "https://api.openweathermap.org/data/2.5/weather?q=$city&appid=$APIKEY&lang=$LANG&units=metric"
        val queue = Volley.newRequestQueue(context)
        val request = StringRequest(
            Request.Method.GET,
            requestURL,
            {
                result -> parseWeatherData(result)
            },
            {
                error -> Log.e("AWeather", "An error occurred while trying to get the weather forecast: $error")
            }
        )
        queue.add(request)

    }

    @SuppressLint("ResourceAsColor", "SetTextI18n")
    private fun parseWeatherData (weatherInfo : String) {
        Log.d("AWeather",weatherInfo)

        val mainObject = JSONObject(weatherInfo)
        val item = NowWeatherModel(
            mainObject.getJSONArray("weather").getJSONObject(0).getString("description").capitalize(),
            mainObject.getJSONArray("weather").getJSONObject(0).getString("main"),
            mainObject.getJSONObject("main").getString("temp"),
            mainObject.getJSONObject("main").getString("feels_like"),
            mainObject.getJSONObject("wind").getString("speed"),
            mainObject.getJSONObject("main").getString("pressure"),
            mainObject.getJSONObject("main").getString("humidity"),
            mainObject.getString("dt"),
            mainObject.getString("timezone"),
            mainObject.getJSONObject("sys").getString("sunrise"),
            mainObject.getJSONObject("sys").getString("sunset"),
            mainObject.getJSONArray("weather").getJSONObject(0).getString("icon"),
        )

        val rdate : String = java.time.format.DateTimeFormatter.ISO_INSTANT.format(java.time.Instant.ofEpochSecond(item.dtResp.toLong() + item.timezone.toLong()))

        val sunrise_date : String = java.time.format.DateTimeFormatter.ISO_INSTANT.format(java.time.Instant.ofEpochSecond(item.sunrise.toLong() + item.timezone.toLong()))
        val sunset_date : String = java.time.format.DateTimeFormatter.ISO_INSTANT.format(java.time.Instant.ofEpochSecond(item.sunset.toLong() + item.timezone.toLong()))

        Log.d("AWeather", "state: ${item.state}")
        Log.d("AWeather", "main: ${item.main}")
        Log.d("AWeather", "currentTemp: ${item.currentTemp}")
        Log.d("AWeather", "feelLike: ${item.feelLike}")
        Log.d("AWeather", "windSpeed: ${item.windSpeed}")
        Log.d("AWeather", "pressure: ${item.pressure.toInt()/1.33333333}")
        Log.d("AWeather", "humidity: ${item.humidity}%")

        binding.cityName.text = city
        binding.CurrentState.text = item.state
        binding.CurrentTemp.text = (item.currentTemp.toDouble()).roundToInt().toString() + "°"
        binding.FeelsLike.text = (item.feelLike.toDouble()).roundToInt().toString() + "°"
        binding.WindSpeed.text = item.windSpeed + " " + getResources().getString(R.string.layoutReplText_windSpeed_val)
        binding.Pressure.text = (item.pressure.toInt()/1.33333333).roundToInt().toString() + " " + getResources().getString(R.string.layoutReplText_atmpressure_val)
        binding.Humidity.text = item.humidity + "%"

        setIcon(item.icon)

        val rhour = (rdate[11] + "" + rdate[12]).toInt()
        val rmin = (rdate[14] + "" + rdate[15]).toInt()

        val sunrise_hour = (sunrise_date[11] + "" + sunrise_date[12]).toInt()
        val sunrise_min = (sunrise_date[14] + "" + sunrise_date[15]).toInt()

        val sunset_hour = (sunset_date[11] + "" + sunset_date[12]).toInt()
        val sunset_min = (sunset_date[14] + "" + sunset_date[15]).toInt()

        Log.d("AWeather", "Last updated: $rhour:$rmin")
        Log.d("AWeather", "Sunrise: $sunrise_hour:$sunrise_min")
        Log.d("AWeather", "Sunset: $sunset_hour:$sunset_min")

        if (rhour == sunset_hour) {
            if (rmin >= sunset_min) setBgScreen(1)
            else setBgScreen(4, item.icon)
        }
        else if (rhour > sunset_hour) setBgScreen(2)
        else if (rhour < sunrise_hour) setBgScreen(2)
        else if (rhour == sunrise_hour) setBgScreen(3)
        else setBgScreen(4, item.icon)
    }

    private fun setIcon(icon : String) {
        when (icon) {
            "01d" -> binding.icon.setImageResource(R.drawable.sun_white)
            "01n" -> binding.icon.setImageResource(R.drawable.moon_white)
            "02d", "02n" -> binding.icon.setImageResource(R.drawable.partly_cloud_white)
            "03d","04d", "03n","04n" -> binding.icon.setImageResource(R.drawable.cloud_white)
            "09d","09n" -> binding.icon.setImageResource(R.drawable.rain_white)
            "10d","10n" -> binding.icon.setImageResource(R.drawable.light_rain_white)
            "11d","11n" -> binding.icon.setImageResource(R.drawable.thunder_white)
            "13d","13n" -> binding.icon.setImageResource(R.drawable.snow)
            "50d","50n" -> binding.icon.setImageResource(R.drawable.mist)
        }
    }

    private fun setBgScreen (vl : Int, icon : String = "00d") {
        when (vl) {
            1 -> {
                Log.d("AWeather", "SUNSET")
                binding.mainScreen.setBackgroundColor(resources.getColor(R.color.sunset))
            }

            2 -> {
                Log.d("AWeather", "NIGHT")
                binding.mainScreen.setBackgroundColor(resources.getColor(R.color.afterSunset))
            }

            3 -> {
                Log.d("AWeather", "SUNRISE")
                binding.mainScreen.setBackgroundColor(resources.getColor(R.color.sunrise))
            }

            4 -> {
                Log.d("AWeather", "DAY")

                when (icon) {
                    "01d" -> binding.mainScreen.setBackgroundColor(resources.getColor(R.color.sunnyDay))
                    "02d" -> binding.mainScreen.setBackgroundColor(resources.getColor(R.color.sunnyDay))
                    "03d" -> binding.mainScreen.setBackgroundColor(resources.getColor(R.color.cloudDay))
                    "04d" -> binding.mainScreen.setBackgroundColor(resources.getColor(R.color.cloudDay))
                    "09d" -> binding.mainScreen.setBackgroundColor(resources.getColor(R.color.rainyDay))
                    "10d" -> binding.mainScreen.setBackgroundColor(resources.getColor(R.color.lightRainDay))
                    "11d" -> binding.mainScreen.setBackgroundColor(resources.getColor(R.color.rainyDay))
                    "13d" -> binding.mainScreen.setBackgroundColor(resources.getColor(R.color.snowDay))
                    "50d" -> binding.mainScreen.setBackgroundColor(resources.getColor(R.color.mistDay))
                }

            }
        }
    }





    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}