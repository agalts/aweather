package com.eagalts.aweather

data class WeatherModel(
    val state: String,
    val currentTemp: String,
    var additionalTemp: String,
    val dt: String,
    val timezone: String,
    val icon: String
)