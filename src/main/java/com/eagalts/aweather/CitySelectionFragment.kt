package com.eagalts.aweather

import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.eagalts.aweather.databinding.FragmentCitySelectionBinding

class CitySelectionFragment : Fragment() {

    private var _binding: FragmentCitySelectionBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentCitySelectionBinding.inflate(inflater, container, false)

        binding.loadingPanel.visibility = View.GONE
        binding.infoMsg.visibility = View.GONE

        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonSecond.setOnClickListener {
            binding.loadingPanel.visibility = View.VISIBLE
            binding.infoMsg.visibility = View.GONE
            findCity(binding.city.text)
        }
    }

    private fun findCity(varCity: Editable) {
        Log.d("AWeather", varCity.toString())
        val requestURL =
            "https://api.openweathermap.org/data/2.5/forecast?q=$varCity&appid=$APIKEY&lang=$LANG&units=metric"
        val queue = Volley.newRequestQueue(context)
        val request = StringRequest(
            Request.Method.GET,
            requestURL,
            {
                val ff = MainFragment()
                ff.setCity(varCity.toString().capitalize())

                binding.loadingPanel.visibility = View.GONE

                findNavController().navigate(R.id.action_SecondFragment_to_FirstFragment)
            },
            {
                Log.w(
                    "AWeather",
                    "City not found: $varCity"
                )

                binding.loadingPanel.visibility = View.GONE
                binding.infoMsg.visibility = View.VISIBLE
            }
        )

        queue.add(request)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
