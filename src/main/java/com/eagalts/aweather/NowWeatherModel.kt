package com.eagalts.aweather

data class NowWeatherModel (
    val state : String,
    val main : String,
    val currentTemp : String,
    val feelLike : String,
    val windSpeed : String,
    val pressure : String,
    val humidity : String,
    val dtResp : String,
    val timezone : String,
    val sunrise : String,
    val sunset : String,
    val icon : String
        )