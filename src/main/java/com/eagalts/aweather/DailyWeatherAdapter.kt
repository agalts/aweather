package com.eagalts.aweather

import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class DailyWeatherAdapter (private val weatherInfo: List<WeatherModel>) : RecyclerView.Adapter<DailyWeatherAdapter.MyViewHolder>() {

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val tvDate: TextView = itemView.findViewById(R.id.tvDate)
        val tvCondition: TextView = itemView.findViewById(R.id.tvCondition)
        val tvTemp: TextView = itemView.findViewById(R.id.tvTemp)
        val tvTempNight: TextView = itemView.findViewById(R.id.tvTempNight)
        val im: ImageView = itemView.findViewById(R.id.im)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.daily_list_item, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.tvDate.text = weatherInfo[position].dt
        holder.tvCondition.text = weatherInfo[position].state
        holder.tvTemp.text = weatherInfo[position].currentTemp
        holder.tvTempNight.text = weatherInfo[position].additionalTemp
        setIcon(holder, weatherInfo[position].icon)
    }

    override fun getItemCount() = weatherInfo.size

    private fun setIcon(holder: MyViewHolder, icon : String) {
        when (icon) {
            "01d","01n" -> holder.im.setImageResource(R.drawable.sun_white)
            "02d", "02n" -> holder.im.setImageResource(R.drawable.partly_cloud_white)
            "03d","04d", "03n","04n" -> holder.im.setImageResource(R.drawable.cloud_white)
            "09d","09n" -> holder.im.setImageResource(R.drawable.rain_white)
            "10d","10n" -> holder.im.setImageResource(R.drawable.light_rain_white)
            "11d","11n" -> holder.im.setImageResource(R.drawable.thunder_white)
            "13d","13n" -> holder.im.setImageResource(R.drawable.snow)
            "50d","50n" -> holder.im.setImageResource(R.drawable.mist)
        }
    }

    /*

    @SuppressLint("ResourceAsColor")
    private fun setBgItem (holder: MyViewHolder, icon : String) {
        when (icon) {
            "01d" -> holder.itemBg.setBackgroundColor(R.color.sunnyDay)
            "02d" -> holder.itemBg.setBackgroundColor(R.color.sunnyDay)
            "03d" -> holder.itemBg.setBackgroundColor(R.color.cloudDay)
            "04d" -> holder.itemBg.setBackgroundColor(R.color.cloudDay)
            "09d" -> holder.itemBg.setBackgroundColor(R.color.rainyDay)
            "10d" -> holder.itemBg.setBackgroundColor(R.color.lightRainDay)
            "11d" -> holder.itemBg.setBackgroundColor(R.color.rainyDay)
            "13d" -> holder.itemBg.setBackgroundColor(R.color.snowDay)
            "50d" -> holder.itemBg.setBackgroundColor(R.color.mistDay)
        }
    }

    */
}