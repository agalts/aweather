
# AWeather

A simple weather application for Android smartphones.


## Tech Stack

**Android Studio:** Dolphin 2021.3.1 Path 1

**Tech-stack:**
- **Kotlin** (1.7.20) - code
- **Android XML** (1.0) - UI
## Installation

- Recommended Android version: 12L and above. 
- Minimum Android version: 9.0 and above

Link to download: [download latest version (0.1)](https://gitlab.com/agalts/aweather/-/blob/main/release/app-release.apk?inline=false "download latest version (1.2)")

    
## Features

- Current weather forecast display:
    - temperature
    - feels like
    - wind speed
    - pressure
    - humidity
- Weather forecast for 24 hours ahead (interval: 3 hours)
- Weather forecast for 5 days ahead (temperature day and night, state)
- Selecting an arbitrary city and viewing the weather in it

In future:
- Saving the history of viewing cities
- Data caching (to speed up the application)
- Additional animations
## Localization


The following languages ​​are supported:
- English *(default)*
- Russian
## API Reference

This application is based on the [**OpenWeatherMap API**](https://openweathermap.org/api) (free plan).

| Parameter | Type     | Description                | Location                |
| :-------- | :------- | :------------------------- | :---------------------- |
| `APIKEY` | `string`  | **Required**. Your API key | **MainFragment.kt**: 19 |




## Screenshot

![App Screenshot](https://gitlab.com/agalts/aweather/-/raw/main/release/Screenshot.png )


## License
The program is distributed with open source code: [MIT](https://choosealicense.com/licenses/mit/)

